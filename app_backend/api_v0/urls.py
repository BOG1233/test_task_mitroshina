from django.urls import path, include
from .views import CommentsBasic
from .views import CommentsSearch


urlpatterns = [
    path('comments/', CommentsBasic.as_view(), name='comments'),
    path('search/', CommentsSearch.as_view(), name='search')
]

