from django.shortcuts import render, HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions
from .models import Comment
from .serializers import CommentSerializer, CommentPostSerializer
from itertools import chain
import re
# Create your views here.


class CommentsBasic(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        order_by = request.GET.get('order_by')
        if order_by:
            comments = Comment.objects.all().order_by(order_by)
        else:
            comments = Comment.objects.all()
        serializer = CommentSerializer(comments, many=True)
        return Response({'comments': serializer.data})

    def post(self, request):
        comment = CommentPostSerializer(data=request.data)
        print(comment)
        if comment.is_valid():
            print(request.user)
            comment.save(my_account=request.user)
            return Response({'code': 200, 'description': 'Ok'})
        else:
            return


class CommentsSearch(APIView):

    def post(self, request):
        my_dict = dict(request.data)

        search = my_dict.get('search')[0]
        order_by = my_dict.get('order_by')[0]
        print(search[0])
        comments = list()
        if search:
            all_comments = Comment.objects.all()
            comments += list(chain(all_comments.filter(blogger_account__icontains=search)))
            comments += list(chain(all_comments.filter(my_account__username__icontains=search)))
            if re.match(r'[1-5]', search):
                comments += list(chain(all_comments.filter(score=int(search))))

            if order_by == '-scope':
                comments.sort(key=lambda x: x.score, reverse=True)
            else:
                comments.sort(key=lambda x: x.score, reverse=False)

        else:
            comments = Comment.objects.all()
        serializer = CommentSerializer(comments, many=True)
        return Response({'comments': serializer.data})

