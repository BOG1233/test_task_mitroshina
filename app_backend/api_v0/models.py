from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Comment(models.Model):
    id = models.AutoField(verbose_name='id', primary_key=True)
    blogger_account = models.CharField(verbose_name='Аккаунт блоггера', max_length=100)
    my_account = models.ForeignKey(User, verbose_name='Мой аккаунт', on_delete=models.CASCADE)
    score = models.IntegerField(verbose_name='Оценка', choices=((1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5')))

    def __str__(self):
        return f"<Comment {self.id}, {self.my_account}>"

    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'
        ordering = ['id']
